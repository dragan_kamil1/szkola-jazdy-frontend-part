$(document).ready(function()
{
    var levelNav = $('.nav').offset().top;

    var stick = function()
    {
        var levelScroll = $(window).scrollTop();

        if (levelScroll > levelNav)
        {
            $('.nav').addClass('sticky');
        } else {
            $('.nav').removeClass('sticky');
        }
    };

    stick();

    $(window).scroll(function() {
        stick();
    });
});
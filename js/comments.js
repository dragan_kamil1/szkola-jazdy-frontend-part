var Comment = React.createClass({

    render: function(){
        console.log('render cComent');
        return (
            <li>{this.props.name}</li>
        )
    }
});
var CommentList = React.createClass({

    render: function(){
        console.log('render cList');
        return(
            <ul>
                {this.props.comments.map(function(comment) {
                        return(
                            <Comment name={comment.name}/>
                        )
                    }

                )}
            </ul>
        )
    }

});

var CommentForm = React.createClass({
    getInitialState: function() {
        console.log('constr cFrom');
         return {value: 'Tu wpisz swoje uwagi'};
        //return null;

    },

    handleChange: function(event) {
        this.setState({value: event.target.value});
    },
    handleSubmit: function (event) {
        //window.alert(tmp);
        this.props.addComment(this.state.value);
        event.preventDefault();
    },
    render: function(){

        console.log('render cForm');
        var value = this.state.value;

        return(
            <form onSubmit={this.handleSubmit.bind(this)}>
                <input type='text'  value={value} onChange={this.handleChange.bind(this)}/>
                <input type="submit" value="Dodaj komentarz"/>
            </form>
        )
    }

});
var CommentSection = React.createClass({

    getInitialState: function() {
        console.log('constr cSection');
        return { comments: [

        ]
        };
    },

        addComment: function(value) {

          var comments = this.state.comments;
            comments.push({name: value});
            this.setState( {comments: comments});

        },

    render: function(){

        console.log('crender cSection');

        return(
            <div>
                <CommentList  comments={this.state.comments} />
                <CommentForm addComment={this.addComment.bind(this)}/>
            </div>
        )
    }
});



React.render(
    <CommentSection />, document.getElementById('comments')
);

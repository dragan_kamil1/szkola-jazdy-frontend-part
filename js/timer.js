/**
 * Created by kamil on 11.11.15.
 */
var TimerExample = React.createClass({

    getInitialState: function(){
        return {HowLongMeMustWait: 0};
    },

    componentDidMount: function(){
        this.timer = setInterval(this.tick, 150);
    },

    componentWillUnmount: function(){
        clearInterval(this.timer);
    },

    tick:function(){
        var MeetingDate = new Date();
        if(this.props.start.getDay() == 5)
        {
            MeetingDate.setDate(MeetingDate.getDate() + 4);
        }
        else if(this.props.start.getDay() == 6)
        {
            MeetingDate.setDate(MeetingDate.getDate() + 3);
        }
        else if(this.props.start.getDay() == 7)
        {
            MeetingDate.setDate(MeetingDate.getDate() + 2);
        }
        else if(this.props.start.getDay() == 1)
        {
            MeetingDate.setDate(MeetingDate.getDate() + 1);
        }
        else if(this.props.start.getDay() == 2 && this.props.start.getHours() < 17)
        {}
        else if(this.props.start.getDay() == 2 && this.props.start.getHours() > 17)
        {
            MeetingDate.setDate(MeetingDate.getDate() + 2);
        }
        else if(this.props.start.getDay() == 3)
        {
            MeetingDate.setDate(MeetingDate.getDate() + 1);
        }
        else if(this.props.start.getDay() == 4 && this.props.start.getHours() < 17 )
        {}
        else if(this.props.start.getDay() == 4 && this.props.start.getHours() > 17)
        {
            MeetingDate.setDate(MeetingDate.getDate() + 5);
        }
        else
        {}
        MeetingDate.setHours(17);
        MeetingDate.setMinutes(0);
        MeetingDate.setSeconds(0);

        this.setState(
            {HowLongMeMustWait: MeetingDate - new Date()}
        );



    },

    render: function(){


        var elapsed = Math.round(this.state.HowLongMeMustWait / 100);
        if(elapsed>0) {
            var seconds = (elapsed / 10).toFixed(1);
            var days = Math.floor(seconds / (60 * 60 * 24));
            seconds = seconds - (days * 60 * 60 * 24);
            var hours = Math.floor(seconds / 60 / 60);
            seconds = seconds - hours * 60 * 60;
            var minutes = Math.floor(seconds / 60);
            seconds = seconds - minutes * 60;
            return <p>
            <b>Wykład rozpocznie się za: {days} dni, {hours} godzin, {minutes} minut, {seconds} sekund</b>
            </p>;
        }
        else
        {
            return <p>
        <b>Wykład właśnie trwa!</b>
        </p>;
        }
    }



});

React.render(
<TimerExample start={new Date()} />,
    document.getElementById('timer')
);
